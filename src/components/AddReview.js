import React, {useState, useEffect} from 'react';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/FontAwesome';

const AddReview = () => {
  const [name, setName] = useState('');
  const [rating, setRating] = useState(0);
  const [comment, setComment] = useState('');
  const [submitting, setSubmitting] = useState(false);

  useEffect(() => {
    (async () => {
      try {
        const reviewerName = await AsyncStorage.getItem('reviewer_name');
        console.log(reviewerName);
        if (reviewerName !== null && reviewerName !== undefined) {
          setName(reviewerName);
        }
      } catch (err) {
        console.err(err);
      }
    })();
  }, []);

  const navigation = useNavigation();
  const close = () => {
    navigation.goBack();
  };
  async function submitReview() {
    setSubmitting(true);

    try {
      await AsyncStorage.setItem('reviewer_name', name);
      const result = await axios.post('http://192.168.0.234:3000/review', {
        name,
        rating,
        comment,
      });
      if (result.status === 200) {
        navigation.goBack();
      }
    } catch (err) {
      console.log('error', err);
    } finally {
      setSubmitting(false);
    }
  }

  return (
    <KeyboardAwareScrollView style={{flex: 1, backgroundColor: '#FFF'}}>
      <View style={styles.root}>
        <TouchableOpacity style={styles.button} onPress={close}>
          <Icon name="close" size={30} color="#0066CC" />
        </TouchableOpacity>

        <Text style={styles.addReview}>Add Review</Text>

        <TextInput
          style={styles.input}
          placeholder="Name (optional)"
          value={name}
          onChangeText={setName}
        />

        <Text style={styles.rating}>Your Rating:</Text>
        <View style={styles.stars}>
          {[1, 2, 3, 4, 5].map(i => {
            return (
              <TouchableOpacity
                onPress={() => setRating(i)}
                style={styles.starButton}
                key={i}>
                <Icon
                  name={'star'}
                  color={rating >= i ? '#FFD64C' : '#CCCCCC'}
                  size={40}
                />
              </TouchableOpacity>
            );
          })}
        </View>

        <TextInput
          style={[styles.input, {height: 100}]}
          placeholder="Review"
          value={comment}
          onChangeText={setComment}
          multiline={true}
          numberOfLines={5}
        />

        {submitting ? (
          <ActivityIndicator
            size="large"
            color="#0066CC"
            style={{padding: 10}}
          />
        ) : (
          <TouchableOpacity style={styles.submitButton} onPress={submitReview}>
            <Text style={styles.submitButtonText}>Submit Review</Text>
          </TouchableOpacity>
        )}
      </View>
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 20,
  },
  button: {
    paddingHorizontal: 10,
  },
  addReview: {
    fontSize: 25,
    color: '#444',
    textAlign: 'center',
    margin: 20,
  },
  input: {
    padding: 10,
    marginVertical: 10,
    marginHorizontal: 20,
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 3,
  },
  rating: {
    fontSize: 20,
    color: 'grey',
    textAlign: 'center',
    marginVertical: 40,
  },
  stars: {
    marginBottom: 80,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  starButton: {
    padding: 5,
  },
  submitButton: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: '#0066cc',
    borderRadius: 4,
    marginVertical: 10,
    marginHorizontal: 20,
  },
  submitButtonText: {
    fontSize: 18,
    color: '#ffffff',
    textAlign: 'center',
  },
});

export default AddReview;
