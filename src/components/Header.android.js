import React from 'react';

import {Text} from 'react-native';
import HeaderStyle from 'styles/HeaderStyle';

export default function Header() {
  return <Text style={HeaderStyle.header}>Restaurant Review</Text>;
}
