import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {View, Image, TextInput, FlatList, StyleSheet} from 'react-native';

import Header from 'components/Header';
import RestaurantRow from 'components/RestaurantRow';
import PizzaImage from 'images/pizza.png';

const RestaurantList = () => {
  const [search, setSearchState] = useState(null);
  const [restaurants, setRestaurants] = useState([]);
  useEffect(() => {
    (async function() {
      const response = await axios.get('http://192.168.0.234:3000/restaurants');
      if (response.status === 200 && response.data) {
        setRestaurants(response.data);
      }
    })();
  }, []);

  return (
    <View style={{flex: 1}}>
      <View
        style={{
          marginTop: 40,
          alignItems: 'center',
        }}>
        <Image source={PizzaImage} />
      </View>
      <Header />
      <TextInput
        style={styles.input}
        placeholder="Live Search"
        onChangeText={text => setSearchState(text)}
      />

      <FlatList
        data={restaurants.filter(item => {
          return (
            !search ||
            item.name.toLowerCase().indexOf(search.toLowerCase()) > -1
          );
        })}
        renderItem={({item, index}) => (
          <RestaurantRow place={item} index={index} />
        )}
        keyExtractor={item => item.name}
        initialNumToRender={16}
        ListHeaderComponent={<View style={{height: 30}} />}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  edges: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
  },
  nameAddress: {
    flexDirection: 'column',
    flex: 8,
  },
  addressText: {
    color: 'grey',
  },
  input: {
    padding: 10,
    paddingHorizontal: 20,
    fontSize: 16,
    color: '#444',
    borderBottomWidth: 1,
    borderColor: '#ddd',
    backgroundColor: '#F5F5F5',
  },
});

export default RestaurantList;
