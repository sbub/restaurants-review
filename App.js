import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import RestaurantList from 'components/RestaurantList';
import RestaurantInfo from 'components/RestaurantInfo';
import About from 'components/About';
import AddReview from 'components/AddReview';

import Icon from 'react-native-vector-icons/FontAwesome';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const ListStack = () => {
  const navigationOptions = {
    headerStyle: {
      backgroundColor: '#0066CC',
    },
    headerTitleStyle: {
      color: '#FFF',
    },
  };
  return (
    <Stack.Navigator screenOptions={navigationOptions}>
      <Stack.Screen
        name="Home"
        component={RestaurantList}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Info"
        component={RestaurantInfo}
        options={{title: 'Restaurant Info'}}
      />
    </Stack.Navigator>
  );
};
const Tabs = () => {
  const navigationOptions = ({navigation, route}) => {
    return {
      tabBarIcon: ({color}) => {
        const routeName = route.name;
        const name = {
          List: 'list',
          About: 'info-circle',
        }[routeName];
        return <Icon name={name} color={color} size={22} />;
      },
      tabBarOptions: {
        activeBackgroundColor: '#E6F0FA',
      },
    };
  };
  return (
    <Tab.Navigator screenOptions={navigationOptions}>
      <Tab.Screen name="List" component={ListStack} />
      <Tab.Screen name="About" component={About} />
    </Tab.Navigator>
  );
};
const AppAndModal = () => {
  const navigationOptions = {
    navigationOptions: {
      gesturesEnabled: false,
    },
  };
  return (
    <NavigationContainer>
      <Stack.Navigator
        mode="modal"
        headerMode="none"
        screenOptions={navigationOptions}>
        <Stack.Screen name="Tabs" component={Tabs} />
        <Stack.Screen name="AddReview" component={AddReview} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppAndModal;

// *** possible compound component implementation ***
// class Stack extends Component {
//   static Navigator = ({...props, children}) => {
//     return React.Children.map(children, childEL => {
//       return React.cloneElement(childEL, {
//         ...props
//       })
//     })
//   }
//   static Screen = ({...props, component}) => {
//     return React.cloneElement(component, {
//       ...props
//     })
//   };
//
//   render() {
//     return null;
//   }
// }
